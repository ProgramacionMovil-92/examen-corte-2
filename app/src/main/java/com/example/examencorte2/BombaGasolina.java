package com.example.examencorte2;

public class BombaGasolina {
  private int id;
  private int numBomba;
  private int capacidadGasolina;
  private int tipoGasolina;
  private float precioGasolina;
  private int acumuladorLitrosBomba;

  public BombaGasolina(int id, int numBomba, int capacidadGasolina, int tipoGasolina, float precioGasolina, int acumuladorLitrosBomba) {
    this.id = id;
    this.numBomba = numBomba;
    this.capacidadGasolina = capacidadGasolina;
    this.tipoGasolina = tipoGasolina;
    this.precioGasolina = precioGasolina;
    this.acumuladorLitrosBomba = acumuladorLitrosBomba;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getNumBomba() {
    return numBomba;
  }

  public void setNumBomba(int numBomba) {
    this.numBomba = numBomba;
  }

  public int getCapacidadGasolina() {
    return capacidadGasolina;
  }

  public void setCapacidadGasolina(int capacidadGasolina) {
    this.capacidadGasolina = capacidadGasolina;
  }

  public int getTipoGasolina() {
    return tipoGasolina;
  }

  public void setTipoGasolina(int tipoGasolina) {
    this.tipoGasolina = tipoGasolina;
  }

  public float getPrecioGasolina() {
    return precioGasolina;
  }

  public void setPrecioGasolina(float precioGasolina) {
    this.precioGasolina = precioGasolina;
  }

  public int getAcumuladorLitrosBomba() {
    return acumuladorLitrosBomba;
  }

  public void setAcumuladorLitrosBomba(int acumuladorLitrosBomba) {
    this.acumuladorLitrosBomba = acumuladorLitrosBomba;
  }
}
